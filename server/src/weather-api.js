const axios = require('axios')
const dotenv = require('dotenv');
dotenv.config();

async function get(lat, lng) {
  try {
    const res = await axios.get(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lng}&units=imperial&exclude=minutely,alerts&appid=${process.env.WEATHER_API_KEY}`)
    return { 
      data: res.data
    }
  } catch (err) {
    // console.log(`Error fetch weather data for location: '${city},${state},${country}' from API`, err)
    return null
  }
}

module.exports = {
  get
}
