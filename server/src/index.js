const createApp = require('./app')

let server
let shuttingDown = false

async function startup() {
  console.log('Initializing express app...')
  return new Promise(resolve => {
    server = createApp().listen(5000, '0.0.0.0', async () => {
      const { address, port } = server.address()
      console.log(`Listening at http://${address}:${port}`)
      resolve()
    })
  })
}

async function shutdown() {
  const timeoutInMilliseconds = 5000
  console.log('Attempting to close the http server...')
  await new Promise(resolve => {
    const timeout = setTimeout(() => {
      console.log(`The http server still has not closed after ${timeoutInMilliseconds} milliseconds. This is likely because of sockets still being open. Proceeding with a forced shutdown.`)
      resolve()
    }, timeoutInMilliseconds)

    server.close(() => {
      clearTimeout(timeout)
      console.log('Http server closed gracefully')
      resolve()
    })
  })
}


async function gracefulShutdown() {
  if (shuttingDown) return
  shuttingDown = true

  console.log('Attempting graceful shutdown...')
  await shutdown()
  setTimeout(() => process.exit(0), 20)
}

function handleError(error) {
  console.error(error)
  process.nextTick(() => process.exit(1))
}

process.on('uncaughtException', handleError)
process.on('unhandledRejection', handleError)
process.on('SIGINT', gracefulShutdown)
process.on('SIGTERM', gracefulShutdown)

startup().catch(err => console.error('Failed to start server', err))
