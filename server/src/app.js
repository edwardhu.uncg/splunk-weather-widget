const express = require('express')
const cors = require('cors')
const weatherApi = require('./weather-api')
const geoCodeApi = require('./geocode-api')
const forwardGeoCodeApi = require('./forwardGeocode-api')
const favicon = require('serve-favicon')
const path = require('path')

function notFound(req, res, next) {
  next(new Error(`There's nothing here at '${req.url}'`))
}

function logErrors(err, req, res, next) {
  console.error(err)
  next(err)
}

function catchAll(err, req, res) {
  const status = err.status || 500
  res.status(status).json(res.failureWithError(err))
}

function wrapAsyncRoute(asyncRoute) {
  return function routeWrapper(req, res, next) {
    return asyncRoute(req, res, next).catch(next)
  }
}

async function fetchWeather(req, res) {
  const data = await weatherApi.get(req.params.lat, req.params.lng)

  if (data) {
    res.status(200).send(data)
  } else {
    res.sendStatus(404)
  }
}

async function fetchLocation(req, res) {
  const data = await geoCodeApi.get(req.params.lat, req.params.lng)

  if (data) {
    res.status(200).send(data)
  } else {
    res.sendStatus(404)
  }
}

async function fetchForwardLocation(req, res) {
  const data = await forwardGeoCodeApi.get(req.params.city, req.params.state)

  if (data) {
    res.status(200).send(data)
  } else {
    res.sendStatus(404)
  }
}

module.exports = () => {
  const app = express()

  app.use(cors())

  app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))

  app.get('/weather/:lat&:lng', wrapAsyncRoute(fetchWeather))
  app.get('/geocode/:lat&:lng', wrapAsyncRoute(fetchLocation))
  app.get('/forwardGeocode/:city&:state', wrapAsyncRoute(fetchForwardLocation))

  app.use(notFound)
  app.use(logErrors)
  app.use(catchAll)

  return app
}
