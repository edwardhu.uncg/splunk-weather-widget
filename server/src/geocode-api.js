const axios = require('axios')
const dotenv = require('dotenv');
dotenv.config();

async function get(lat, lng) {
  try {
    const res = await axios.get(`https://api.opencagedata.com/geocode/v1/json?q=${lat}+${lng}&key=${process.env.GEOCODE_API_KEY}`)
    return { 
      results: res.data.results[0],
    }
  } catch (err) {
    // console.log(`Error fetch geo data for location: '${lat},${lng}' from API`, err)
    return null
  }
}

module.exports = {
  get
}
