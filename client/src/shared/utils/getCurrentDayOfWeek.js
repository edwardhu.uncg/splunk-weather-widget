const getCurrentDayOfWeek = function(start, abbr) {
  let curDayOfWeek = '';
  let from  = start ? start : 0;
  const now = new Date();
  const dayOfWeek = now.getDay();

  switch(dayOfWeek + from > 7 ? dayOfWeek + from - 7 : dayOfWeek + from) {
    case 1:
      curDayOfWeek = abbr ? 'Mon' : 'Monday';
      break;
    case 2:
      curDayOfWeek = abbr ? 'Tue' : 'Tuesday';
      break;
    case 3:
      curDayOfWeek = abbr ? 'Wed' : 'Wednesday';
      break;
    case 4:
      curDayOfWeek = abbr ? 'Thu' : 'Thursday';
      break;
    case 5:
    curDayOfWeek = abbr ? 'Fri' : 'Friday';
      break;
    case 6:
      curDayOfWeek = abbr ? 'Sat' : 'Saturday';
      break;
    case 7:
      curDayOfWeek = abbr ? 'Sun' : 'Sunday';
      break;
    default:
      curDayOfWeek = abbr ? 'Mon' : 'Monday';
      break;
  }
  return curDayOfWeek;
}

export default getCurrentDayOfWeek;