const xAxisLabels = (() => {
  let labelsArr = [''];
  const nextHour = new Date().getHours() + 1;
  const nextHourLabel = nextHour + ':00';
  labelsArr.push(nextHourLabel);
  let counter = 0;

  for(let i = 2; i < 24; i++) {
    if(counter === 2) {
      let hr = nextHour + i - 1;
      let unit = ':00';
      if ( hr > 12) {
        hr %= 12;
        if(hr === 0) {
          hr = 12;
        }
      } 
      labelsArr.push(hr + unit);
      counter = 0;
    } else { 
      labelsArr.push('');
      counter++;
    }
  }

  return labelsArr;
})()

export const weatherChartDefaultOption = {
  chart: {
    type: "areaspline",
    height: 200,
    animation: false
  },
  title: {
    text: ''
  },
  xAxis: {
    crosshair: false,
    tickLength: 0,
    categories: xAxisLabels,
  },
  yAxis: {
    visible: false
  },
  legend: {
    enabled: false
  },
  credits: {
    enabled: false
  },
  plotOptions: {
    area: {
      stacking: "normal",
      lineColor: "#ffffff",
      lineWidth: 2
    },
    series: {
      marker: {
        enabled: false
      }
    }
  },
  series: [
    {
      name: '',
      data: [],
      color: '#ffc8ba',
      lineColor: '#e96e50',
      lineWidth: 3,
      dataLabels: {
        enabled: true,
        color: '#909090'
      },
      events: {
        legendItemClick: function() {
          return false;
        }
      },
      states: {
        hover: {
          enabled: false
        }
      }
    }
  ],
  tooltip: {
    enabled: false
  }
}