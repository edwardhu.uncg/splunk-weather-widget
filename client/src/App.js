import React from "react"

import "./App.css"
import WeatherCard from './components/weatherCard/WeatherCard';

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <WeatherCard />
    )
  }
}
