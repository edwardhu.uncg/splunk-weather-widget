import React from 'react';
import './WeatherCard.scss';

class MiniWeatherCard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div className='mini-card-wrapper'>
        <ul>
          <li className='mini-card-weather-description'>{ this.props.data.weather[0].description}</li>
          <li className='mini-card-weather-temp'>{ this.props.data.temp.min }&#730; - { this.props.data.temp.max}&#730;</li>
          <li className='mini-card-weather-temp-detail'>
            <div className='detail-card'>
              <p className='detail-title'>Day</p>
              <p><b>{ this.props.data.feels_like.day }&#730;</b></p>
            </div>
            <div className='detail-card'>
              <p className='detail-title'>Evening</p>
              <p><b>{ this.props.data.feels_like.eve }&#730;</b></p>
            </div>
            <div className='detail-card'>
              <p className='detail-title'>Morning</p>
              <p><b>{ this.props.data.feels_like.morn }&#730;</b></p>
            </div>
            <div className='detail-card'>
              <p className='detail-title'>Night</p>
              <p><b>{ this.props.data.feels_like.night }&#730;</b></p>
            </div>
          </li>
          <li className='mini-card-weather-humidity'>Humidity: <b>{ this.props.data.humidity }</b></li>
          <li className='mini-card-weather-wind'>Wind Speed: <b>{ this.props.data.wind_speed }</b></li>
        </ul>
      </div>
    )
  }
}

export default MiniWeatherCard;