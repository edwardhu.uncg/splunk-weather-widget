import React from 'react';
import errorHandler from '../../shared/utils/errorHandler';
import getStateAbbr from '../../shared/utils/getStateAbbr';
import getCurrentDayOfWeek from '../../shared/utils/getCurrentDayOfWeek';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { weatherChartDefaultOption } from '../../shared/utils/defaultWeatherChartOption';
import MiniWeatherCard from './MiniWeatherCard';
import csc from 'country-state-city';
import { Animated } from "react-animated-css";

import './WeatherCard.scss';

class WeatherCard extends React.Component {
  
  constructor(props) {
    super(props)

    this.state = {
      currentTime: '',
      weather: {
        current: {
          weather: [{
            id: 0,
            description: '',
            icon: '10d',
            main: ''
          }]
        },
        daily: null,
        hourly: null
      },
      errMsg: '',
      position: {
        lat: 42.366455699999996,
        lng: -71.0854208
      },
      location: {
        city: 'BOSTON',
        state: 'MA'
      },
      dailyCardIdx: 0,
      chartOption: weatherChartDefaultOption,
      showNextDayWeather: false,
      showPermissionWarning: false,
      states: csc.getStatesOfCountry('US'),
      selectedState: '',
      cities: [],
      selectedCity: '',
      showLocationEditor: false,
      showMask: true
    }
  }
  
  componentDidMount() {
    if(navigator.geolocation) {
      navigator.permissions.query({ name: 'geolocation' }).then(permissions => {
        if (permissions.state === 'granted') {
          navigator.geolocation.getCurrentPosition((position) => {
            this.setState({ position: position });
            this.fetchLocation(position.coords.latitude, position.coords.longitude);
            this.fetchWeather(position.coords.latitude, position.coords.longitude);
          });
        } 
        else {
          this.setState({ showPermissionWarning: true, showLocationEditor: true });
          this.fetchWeather(this.state.position.lat, this.state.position.lng);
        }
      })
    } else {
      errorHandler('Geolocation is not supported by this browser.');
    }

    
    this.interval = setInterval(() => {
      this.getTime()
    }, 1000)
  }

  componentDidUpdate(prepProps, prevState) {
    if( prevState.weather.hourly !== this.state.weather.hourly) {
      if(Array.isArray(this.state.weather.hourly)) {
        const tempArr = this.getHourlyTemp(this.state.weather.hourly);
        const updatedSeries = weatherChartDefaultOption.series;
        updatedSeries[0].data = tempArr;

        this.setState({
          chartOption: {
            ...this.state.chartOption,
            series: updatedSeries
          }
        })
      }
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  fetchWeather(lat, lng) {
    this.setState({ showMask: true })
    fetch(`http://localhost:5000/weather/${lat}&${lng}`)
      .then(res => {
        if (res.ok) {
          return res.json()
        } else {
          this.setState({ errMsg: 'Weather fetching error.' })
        }
      })
      .then(
        weather => {
          this.setState({ 
            weather: {
              current: weather.data.current,
              daily: weather.data.daily.slice(0, 5),
              hourly: weather.data.hourly.slice(0, 24)
            },
            showMask: false
          })
        },
        (error) => {
          errorHandler(error)
        }
      )
  }

  fetchLocation(lat, lng) {
    fetch(`http://localhost:5000/geocode/${lat}&${lng}`)
      .then(res => {
        if (res.ok) {
          return res.json()
        } else {
          this.setState({ errMsg: 'Location fetching error.' })
        }

      })
      .then(
        pos => {
          this.setState({ 
            location: {
              city: pos.results.components.city,
              state: pos.results.components.state,
            } 
          })
        },
        (error) => {
          errorHandler(error)
        }
      )
  }

  fetchForwardLocation(city, state) {
    fetch(`http://localhost:5000/forwardGeocode/${city}&${state}`)
      .then(res => {
        if (res.ok) {
          return res.json()
        } else {
          this.setState({ errMsg: 'Location fetching error.' })
        }

      })
      .then(
        pos => {
          if(pos.results.geometry) {
            this.setState({ 
              position: {
                lat: pos.results.geometry.lat,
                lng: pos.results.geometry.lng,
              } 
            })
            this.fetchWeather(pos.results.geometry.lat, pos.results.geometry.lng)
          }
        },
        (error) => {
          errorHandler(error)
        }
      )
  }


  getHourlyTemp(hourlyArr) {
    let tempArr = [];

    hourlyArr.forEach(data => {
      tempArr.push(Math.ceil(data.temp))
    });

    return tempArr;
  }

  getTime() {
    const d = new Date();
    const m = d.getMinutes();
    const h = d.getHours();
    this.setState({ currentTime: ("0" + h).substr(-2) + ":" + ("0" + m).substr(-2) })
  }

  getWeatherIcon(id) {
    return `http://openweathermap.org/img/wn/${id}@2x.png`;
  }

  dailyCardOnClick(idx) {
    this.setState({ dailyCardIdx: idx });
    if(idx !== 0) {
      this.setState({ showNextDayWeather: true });
    } else {
      this.setState({ showNextDayWeather: false });
    }
  }

  closeNextDayCard() {
    this.setState({ showNextDayWeather: false });
  }

  stateSelected(e) {
    this.setState({ selectedState: e.target.value });
    const cities = csc.getCitiesOfState('US', e.target.value);
    this.setState({ cities: cities });
    e.preventDefault();
  }

  citySelected(e) {
    this.setState({ selectedCity: e.target.value });
    e.preventDefault();
  }

  changeLocation() {
    this.setState({ 
      location: {
        city: this.state.selectedCity,
        state: this.state.selectedState
      }, 
      showLocationEditor: false,
      showNextDayWeather: false,
      dailyCardIdx: 0,
      showMask: true
    })
    this.fetchForwardLocation(this.state.selectedCity, this.state.selectedState);
  }

  render() {
    return(
      <>
      { this.state.weather ? (
        <>
          <div className='card-wrapper'>
            <div className='top-info-wrapper'>
              <div className='top-left-info-block'>
                <div className='general-info-wrapper'>
                  { !this.state.showPermissionWarning && 
                    <div className='info-title-wrapper'>
                      <p className='location'>{ this.state.location.city } { getStateAbbr(this.state.location.state) }</p>
                      { !this.state.showLocationEditor && <span className='info-edit-btn' onClick={ () => { this.setState({ showLocationEditor: true }) } }>[Edit]</span> }
                      { this.state.showLocationEditor && <span className='info-edit-btn' onClick={ () => { this.setState({ showLocationEditor: false }) } }>[&#x2715;]</span> } 
                    </div>
                  }
                  { this.state.showPermissionWarning && 
                    <>
                      <p className='location'>{ this.state.location.city } { this.state.location.state }</p> 
                    </>
                  }
                  { this.state.showLocationEditor && 
                    <>
                      <select name="getStates" onChange={ (e) => this.stateSelected(e) } value={ this.state.selectedState }>
                        { this.state.states.map((state, idx) => (
                          <option value={state.isoCode} key={ idx }>{state.isoCode}</option>
                        ))}
                      </select>
                      { this.state.selectedState !== '' && this.state.cities.length > 0 && 
                        <select name="getCities" onChange={ (e) => this.citySelected(e) }>
                          { this.state.cities.map((city, idx) => (
                            <option value={city.name} key={ idx }>{city.name}</option>
                          ))}
                        </select>
                      }
                      { this.state.selectedCity && <button className='location-editor-submit-btn' onClick={ () => this.changeLocation() }>Change Location</button> }
                    </>
                  }
                  <p className='date-time'>{ getCurrentDayOfWeek() } { this.state.currentTime }</p>
                </div>
                <div className='weather-overview'>
                  <p className='weather'>{ this.state.weather.current.weather[0].description }</p>
                  <div className='icon-tmp-wrapper'>
                    <div className='icon-wrapper'>
                      <img src={ this.getWeatherIcon(this.state.weather.current.weather[0].icon) } alt='today-weahter-icon' />
                    </div>
                    <div className='tmp-wrapper'>
                      <p className='degree'>{ Math.ceil(this.state.weather.current.temp) }</p>
                      <p className='tmp-unit'>&#730;F</p>
                    </div>
                  </div>
                </div>  
              </div>
              { this.state.showNextDayWeather && 
                <Animated animationIn="bounceInDown" isVisible={true}>
                  <div className='top-right-info-block'>
                    <div className='mini-weather-card-navigator'>
                      <div className='mini-weather-card-title'>{ getCurrentDayOfWeek(this.state.dailyCardIdx, false) }</div>
                      <div className='mini-weather-card-exit-btn' onClick={ () => this.closeNextDayCard() }> &#x2715; </div>
                    </div>
                    <MiniWeatherCard data={ this.state.weather.daily[this.state.dailyCardIdx]} />
                  </div>
                </Animated>
              } 
            </div>
            <div>
              <HighchartsReact highcharts={ Highcharts } options={ this.state.chartOption }/>
            </div>
            <div className='weekly-forecast-wrapper'>
              { this.state.weather.daily && this.state.weather.daily.map((day, index) => (
                <div key={ index } 
                  onClick={ () => this.dailyCardOnClick(index) }
                  className={`daily-card ${index === this.state.dailyCardIdx ? 'daily-card-selected' : null}`} 
                >
                  <p className='daily-card-title'> { getCurrentDayOfWeek(index, true) } </p>
                  <div className='daily-card-icon'>
                    <img src={ this.getWeatherIcon(day.weather[0].icon) } alt='daily-weahter-icon'/>
                  </div>
                  <div className='daily-card-tmp-wrapper'>
                    <p className='daily-highest-temp'>{ Math.ceil(day.temp.max) }&#730;</p>
                    <p>{ Math.ceil(day.temp.min) }&#730;</p>
                  </div>
                </div>
              ))}
            </div>
            { this.state.showMask && 
              <div className='loading-layer'>
                <Animated animationIn="bounce" isVisible={true}>
                    <div>
                        Loading...
                    </div>
                </Animated>
              </div>
            }
          </div>
        </>
      ) : null }
      </>
    )
  }
}

export default WeatherCard;