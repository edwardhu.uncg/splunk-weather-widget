# Installation #
## Server ##
This assumes at least node 10 is installed
```
cd server
```
#### Install ####
```
npm install
```
## Client ##
```
cd ../client
```
#### Install ####

```
npm install
```


# Launch Application #
#### Start the Client + Server Concurrently ####
CD to the root directory. The client will run on port: **3000**. (http://localhost:3000)
```
cd ../
```
```
npm run start
```
#### Launch the API Only ####
The server will run on port: **5000**. (http://localhost:5000)
```
npm run start-server
```
